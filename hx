#! /usr/bin/env python3

import sys
import os
import argparse
import signal

def signal_handler(signum, frame):
    """
    exit gracefully
    """

    print("User interrupt.")
    if not args.file == '-':
        bytestream.close()
    exit(255)
    
def grabchar(strm, prevc=None):
    """
    grab one char from input stream, attempt decoding it and grab more
    recursively if the character is multi-byte.
    """

    inbyte = strm.read(1)

    if prevc is not None:
        inbyte = prevc + inbyte
    try:
        c = inbyte.decode(args.encoding)
    except UnicodeDecodeError:
        inbyte = grabchar(strm, inbyte)
        
    return inbyte

def dumpblock(linesblock, maxl):
    """
    print out current block of lines
    """
    
    charfmt = f"%{padchar}{maxl*2}x"   # multi-byte representation
    eofmark = (maxl*2 - 1) * '_' + '$' # end of file mark in bytestream block
    addrfmt = "%04x"                   # offset format
    linefmt = f"%-{args.line_width * (maxl*2 + 1)}s" # bytestream line format
    lncnt = 1                          # line count for pagination

    # bytestream block line end depends on whether there is a glyph block
    if args.no_chars:
        lineblk_end = '\n'
    else:
        lineblk_end = args.block_sep

    # print out look-ahead block, one line at a time
    for (addr, lndata) in linesblock:
        lncnt += 1
        
        # print offset
        if not args.no_offset:
            print(addrfmt % addr, end=args.block_sep)

        # build and print bytestream
        lineblock = ''
        charblock = ''
        for c in lndata:
            # build characters block
            if c < b' ':
                charblock += '.'
            else:
                charblock += c.decode(args.encoding)

            # builds bytestream dump block
            try:
                hexval = charfmt % c
            except TypeError:
                hxval = c.hex()
                while len(hxval) < maxl * 2:
                    if padchar == '':
                        hxval = '  ' + hxval
                    else:
                        hxval = "00" + hxval
            lineblock += hxval + ' '

        # too short a line indicates EOF
        if len(lndata) < args.line_width:
            lineblock += eofmark
            
        print(linefmt % lineblock, end=lineblk_end)

        # print characters glyphs block
        if not args.no_chars:
            print(charblock)

        # pagination if requested
        if (args.page > -1) and (lncnt == args.page):
            input('Press enter of interrupt...')
            lncnt = 1

"""
main starts here
"""

# capture keyboard interrupt
#
signal.signal(signal.SIGINT, signal_handler)

# set up arguments parser
#
parser = argparse.ArgumentParser(description='Hex dump')
parser.add_argument('file', help='filename, or - for stdin')
parser.add_argument('-b', '--block-sep',
                    help='Visual separator between blocks',
                    default='|')
parser.add_argument('-c', '--no-chars',
                    help='Do not display character representation block',
                    action='store_true')
parser.add_argument('-e', '--encoding',
                    help='Use specified text encoding',
                    default='utf-8')
parser.add_argument('-o', '--no-offset',
                    help='Do not display file offset block',
                    action='store_true')
parser.add_argument('-p', '--page',
                    help='Paginate at the specified number of lines, or one screenful (ignored if reading from stdin)',
                    nargs='?',
                    type=int,
                    default=-1,
                    const=0)
parser.add_argument('-s', '--space',
                    help='Hex values padded with space instead of zeroes',
                    action='store_true')
parser.add_argument('-w', '--line-width',
                    help='Characters (multi-byte chars counted as one) per line',
                    type=int,
                    default=16)
parser.add_argument('-x', '--max-char',
                    help='Max character width is known to be this many bytes',
                    default=0,
                    type=int)
args = parser.parse_args(sys.argv[1:])

# validate arguments
#

# valid encoding requested ?
try:
    c = b' '.decode(args.encoding)
except ( UnicodeDecodeError, LookupError ):
    print(f"Invalid encoding {args.encoding}, assuming utf-8.")
    args.encoding = 'utf-8'

# input file ?
if args.file == '-':
    bytestream = sys.stdin.buffer
else:
    try:
        bytestream = open(args.file, 'rb')
    except FileNotFoundError:
        print(f"Cannot find {args.file}.")
        exit(1)
    except PermissionError:
        print(f"Cannot read {args.file}.")
        exit(2)

# pad character
if args.space:
    padchar=''
else:
    padchar='0'

# pagination
if not args.page == -1:
    try:
        (consr, consl) = os.get_terminal_size()
        args.page = consl
    except OSError:
        args.page = -1
    if args.file == '-':
        # ignore pagination option if reading from stdin
        args.page = -1
        
# begin processing
#

eof = False                 # stop condition
offs = 0                    # current file offset
lookahead =  16 if args.page == -1 else args.page # this many lines to preload
lablock = []                # look ahead block : sequential array of lines
maxcharlen = args.max_char  # widest char encountered
lablockidx = 0              # position in look-ahead block

while not eof:
    cntchar = 0    # characters read so far on current line block
    cntbytes = 0   # bytes read so far on current line block
    linebytes = [] # line block buffer

    while cntchar < args.line_width:
        # grab next character in stream, possibly multibyte
        nextchar = grabchar(bytestream)
        charln = len(nextchar)
        if charln == 0:
            # we're done
            eof = True
            break

        maxcharlen = max(maxcharlen, charln) # widest char so far ?
        linebytes.append(nextchar) # fill look-ahead line
        cntchar += 1
        cntbytes += charln
        
    # current line done, update look-ahead block
    lablock.append( (offs,linebytes) )
    offs += cntbytes
    lablockidx += 1
    if lablockidx == lookahead:
            # look-ahead buffer full, print out and clear
            dumpblock(lablock, maxcharlen)
            lablockidx = 0
            del lablock[:]
            
    if eof:
        dumpblock(lablock, maxcharlen)
        break

bytestream.close()
