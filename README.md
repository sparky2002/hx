# hx

*hx* is a variant of the standard hexdump program.

Contrary to hexdump which reads and displays a raw bytestream, with focus on the bytes, hx tries to interpret the stream according to the given encoding as it reads, and will display multibytes characters as a unit.

## Overview

*hx* will read from the specified filename, or standard input if invoked with '-'.

It will display the hexadecimal values of the raw bytestream, grouping the bytes according to the specified encoding (utf-8 by default) when encountering multi-byte characters.

By default, it displays the current line offset, the hexadecimal dump of characters as well as the glyph representation of the characters from the stream.

It should be noted that the offsets are not necessarily regular, since the number of bytes per line does not always match the number of characters.

The width of the hexadecimal representation self-adjusts to the widest (in bytes) character encountered.

## Requirements
* Python 3
* Standard python modules: sys, os, signal
* argparse python module

## Installation
This utility does not require any installation. Copy wherever you like and call it through the python interpreter, or mark it as executable.

Note: the script assumes `/usr/bin/env` to search for the `python3` interpreter. Adjust the shebang line as needed.

## Usage

```
usage: hx [-h] [-b BLOCK_SEP] [-c] [-e ENCODING] [-o] [-p [PAGE]] [-s] [-w LINE_WIDTH] [-x MAX_CHAR] file

Hex dump

positional arguments:
  file                  filename, or - for stdin

optional arguments:
  -h, --help            show this help message and exit
  -b BLOCK_SEP, --block-sep BLOCK_SEP
                        Separator between blocks
  -c, --no-chars        Do not display character representation block
  -e ENCODING, --encoding ENCODING
                        Use specified text encoding
  -o, --no-offset       Do not display file offset block
  -p [PAGE], --page [PAGE]
                        Paginate at the specified number of lines, or one screenful (ignored if reading from stdin)
  -s, --space           Hex values padded with space instead of zeroes
  -w LINE_WIDTH, --line-width LINE_WIDTH
                        Characters (multi-byte chars counted as one) per line
  -x MAX_CHAR, --max-char MAX_CHAR
                        Max character width is known to be this many bytes

```

## Options
### --no-offset, --no-chars
Do not display the offset block or the glyph block, respectively.
### --space
By default, characters with fewer bytes than the encountered maximum are displayed padded with zeroes up to the required length; with this option spaces are used instead.
### --line-width
This specifies the length *in displayable characters* of the dump line. A given position may very well show a multi-byte character.
### --max-chars
*hx* reads the bytestream with a few lines of look-ahead (typically, one screenful) and determines the maximum character length based on what is found within this look-ahead. Consequently, the display width might change mid-file as it encounters wider characters.

This option allows to force a given character width from the start.

## Sample output
Displaying a sample UTF-8 text, with the widest character 3 bytes long:

```console

    01ae | 000061 000074 000069 000063 000073 000020 000061 00006e 000064 000020 000073 000063 000069 000065 00006e 000063  | atics and scienc
    01be | 000065 000073 00003a 00000a 00000a 000020 000020 e288ae 000020 000045 e28b85 000064 000061 000020 00003d 000020  | es:..  ∮ E⋅da = 
    01d2 | 000051 00002c 000020 000020 00006e 000020 e28692 000020 e2889e 00002c 000020 e28891 000020 000066 000028 000069  | Q,  n → ∞, ∑ f(i

```
